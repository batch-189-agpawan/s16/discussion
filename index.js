let firstNumber = 12
let secondNumber = 5
let total = 0

// ARITHMETIC OPERATORS
// Addition Operator - responsible for adding two or more numbers
total = firstNumber + secondNumber
console.log('Result of addition operator is ' + total)

// Subtraction Operator - responsible for subtracting two or more numbers
total = firstNumber - secondNumber
console.log('Result of subtraction operator is ' + total)

// Multiplication Operator - responsible for multiplying two or more numbers
total = firstNumber * secondNumber
console.log('Result of multiplication operator is ' + total)

// Division Operator - responsible for dividing two or more numbers
total = firstNumber / secondNumber
console.log('Result of division operator is ' + total)

// Modulo Operator - responsible for getting the remainer of two or more numbers
total = firstNumber % secondNumber
console.log('Result of modulo operator is ' + total)

// ASSIGNMENT OPERATORS
// Reassignment Operator - should be a single equals sign, signifies re-assignment or new value into existing variable
total = 27
console.log('Result of reassignment operator is ' + total)

// Addition Assignment Operator - uses the current value of the variable and ADDS a number to itself. Afterwards, reassigns it as the new value
total += 3
// total = total + 3
console.log('Result of addition operator is ' + total)

// Subtraction Assignment Operator - uses the current value of the variable and SUBTRACTS a number to itself. Afterwards, reassigns it as the new value
total -= 5
// total = total - 5
console.log('Result of subtraction operator is ' + total)

// Multiplication Assignment Operator - uses the current value of the variable and MULTIPLIES a number to itself. Afterwards, reassigns it as the new value
total *= 4
// total = total * 4
console.log('Result of multiplication operator is ' + total)

// Division Assignment Operator - uses the current value of the variable and DIVIDES a number to itself. Afterwards, reassigns it as the new value
total /= 20
// total = total - 20
console.log('Result of division operator is ' + total)


// MULTIPLE OPERATORS
let total2 = 0

/*
When doing multiple operations, the program the MDAS rule
MDAS (multiply, divide, add, subtract)
*/
total2 = 2 + 1 - 5 * 4 / 1
console.log(total2)

/*
When doing multiple operations, the program the PEMDAS rule
PEMDAS (Parenthesis, exponent, multiply, divide, add, subtract)
*/

// Exponents are declared by using double asterisks(**) after the number
pemdasTotal = 5**2 + (10 - 2) / 2 * 3
console.log(pemdasTotal)


// INCREMENT AND DECREMENT OPERATORS
let incrementNumber = 1
let decrementNumber = 5

/*
	Pre-increment - adds 1 first before reading value
	pre-Decrement - subtract 1 first before reading value

	Post-increment - reads value first before adding 1 
	Post-decrement - reads value first before subtracting 1
*/

let resultofPreIncrement = ++incrementNumber
let resultofPreDecrement = --decrementNumber
let resultofPostIncrement = incrementNumber++ 
let resultofPostDecrement = decrementNumber--

console.log(resultofPreIncrement)
console.log(resultofPreDecrement)
console.log(resultofPostIncrement)
console.log(resultofPostDecrement)

// Coercion - add a string to a non-string value
let a = '10'
let b = 10

console.log(a + b)

// Non-coercion - add 2 or more non-string values
let d = 10
let f = 10

console.log(d + f)

// Typeof Keyword - returns the data type of a variable
let stringType = 'hello' 
let numberType = 1
let booleanType = true
let arrayType = ['1', '2', '3']
let objectType = {
	objectkey: 'Object value'
}

console.log(typeof stringType)
console.log(typeof numberType)
console.log(typeof booleanType)
console.log(typeof arrayType)
console.log(typeof objectType)

// computer read 'true' as 1
// computer reads 'false' as 0
console.log(true + 1)
console.log(false + 1)

// COMPARISON OPERATORS
// Equality Operator - checks if both values are the same, returns true if it is
let number5 = 5
console.log(number5 == 5)
console.log('hello' == 'hello')
console.log(2 == '2')

// Strict Equality Operator - checks if both values AND data types are the same, returns true if it is
console.log(2 === '2')
console.log(true === 1)

// Inequality Operator - checks if both values are NOT equal, returns true if they aren't
console.log(5 != 5)
console.log('hello' != 'hello')
console.log(2 != '2')

// Strict Inequality Operator - checks if both valuies AND data types are NOT equal, return true if the aren't
console.log(1 !== '1')


// RELATIONAL OPERATORS
let firstVariable = 5
let secondVariable = 5

console.log(firstVariable > secondVariable)
console.log(firstVariable < secondVariable)

console.log(firstVariable >= secondVariable)
console.log(firstVariable <= secondVariable)


// LOGICAL OPERATORS
let isLegalAge = true
let isRegistered = false

// AND operator - returns true if both statements are true. returns false if one of them is not true
console.log(isLegalAge && isRegistered)

// OR operator - returns true if only one of the statements are true. returns false if NONE of the statements are true
console.log(isLegalAge || isRegistered)

// NOT operator - reverses the boolean value (from true to false v/v)
console.log(!isRegistered)

// test
console.log(isLegalAge && !isRegistered)


// Truthy and Falsey values
// everything that is either empty, zero or null will equate to false, and everyhing that has some sort of value will equate to true
console.log([] == false)
console.log('' == false)
console.log(0 == false)

